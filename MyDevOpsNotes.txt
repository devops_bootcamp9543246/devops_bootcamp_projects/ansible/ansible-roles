-------------------------------------------------------------------------------------------------------------------------
Ansible Roles - Make your Ansible content more reusable and modular
-------------------------------------------------------------------------------------------------------------------------
- You can group your content in roles to easily reuse and share them with other users 
- Break up large Playbooks into smaller manageable files 

Usage of roles in your play 
Roles can contain 
- Tasks that the role executes 
- Static files that the role deploys 
- (Default) variables for the tasks (which you can overwrite) 
- Custom modules, which are used within this role 

- Roles are like small applications
- Easy to maintain and reuse 
- Develop and test separately 

Standard file structure 
- A role has a defined directory structure 
- So it's easy to navigate and maintain 
Use existing roles 
- Write own roles or use existing ones from community 
- Download from Ansible Galaxy or Git repository 
Like functions 
- Extract common logic 
- Use function/role in different places with different parameters 

Introduction
What are Roles in Ansible?
What are the Use Cases?
Demo: Create own Role

Automate our Infrastructure
Automate our Networks
Automate our Applications

Group your content in roles
Break up large playbooks into smaller manageable files
To easily reuse and share them with others

Where to get these roles from community?
Ansible Galaxy
Git Repository

Create Ansible Roles

How to create Roles?
How to user Roles in your Playbook?

Like Functions
- extract common logic
- use function in different places with different parameters


Using Roles in Plays

Complete our Roles

Customize Roles with Variables

Useful Links:
● Official Doc:
https: //docs.ansible.com/ansible/latest /user_guide/playbooks_reuse_roles.html
● Variable Precedence:
https: //docs.ansible.com/ansible/latest /user_guide/playbooks_variables.html#vari able-precedence-where-should-i-put-a-variable
